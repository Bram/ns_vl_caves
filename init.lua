-- CONSTANTS

local modname = minetest.get_current_modname()
local ORE_COAL = {
    name = "coal",
    mined = "mcl_core:coal_lump",
    molten = "mcl_core:coal_lump",
    pick_level = 1,
    xp = 1,
    fortune = true,
}
local ORE_DIAMOND = {
    name = "diamond",
    mined = "mcl_core:diamond",
    molten = "mcl_core:diamond",
    pick_level = 4,
    xp = 4,
    fortune = true,
}
local ORE_IRON = {
    name = "iron",
    mined = "mcl_raw_ores:raw_iron",
    molten = "mcl_core:iron_ingot",
    pick_level = 3,
    xp = 0,
    fortune = true,
}
local ORE_OSMIUM = {
    name = "osmium",
    mined = "ns_vl_caves:raw_osmium",
    molten = "ns_vl_caves:osmium_ingot",
    pick_level = 4,
    xp = 8,
}
local ORE_URANIUM = {
    name = "uranium",
    mined = "ns_vl_caves:raw_uranium",
    molten = "ns_vl_caves:uranium_ingot",
    pick_level = 4,
    xp = 4,
    fortune = true,
    extra_params = { light_source = 7 },
}

local STONE_MIN = -100
local DEEPSLATE_MAX = STONE_MIN - 1
local DEEPSLATE_MIN = -1000
local LEAGUESCHIST_MAX = DEEPSLATE_MIN - 1
local LEAGUESCHIST_MIN = -2000
local BANDGNEISS_MAX = LEAGUESCHIST_MIN - 1
local BANDGNEISS_MIN = -3000
local WORLD_DEPTH = BANDGNEISS_MIN


-- Does that sound ominous? It better does! We're overwriting this!
local OLD_REGISTER_BIOME = minetest.register_biome

local internal = {}

-- Run once on initialization
function internal.init()
    -- Set the world's depth
    internal.overworld_depth_to(WORLD_DEPTH)

    -- Add custom caves
    internal.add_custom_caves("mapgen.lua")

    -- Remove massive v7 caves
    internal.stop_v7_caverns()

    -- Overwrite register_biome so underground biomes are separated
    internal.overwrite_register_biome()

    -- Define new ores
    internal.register_ore({
        name = "Uranium",
        hardness = 6,
        extra_params = { light_source = minetest.LIGHT_MAX },
        extra_raw_params = { light_source = 7 },
    })
    internal.register_ore({ name = "Osmium", hardness = 9 })

    -- Define new deepslate layer nodes
    internal.register_stone({
        name = "Leagueschist",
        description = "Leagueschist is a type of stone that can be found very far down underground.",
        hardness = 6,
        ores = { ORE_COAL, ORE_IRON, ORE_DIAMOND, ORE_URANIUM },
    })

    internal.register_stone({
        name = "Bandgneiss",
        description = "Bandgneiss is a hard compressed type of stone that can be found extremely far down underground.",
        hardness = 9,
        ores = { ORE_DIAMOND, ORE_URANIUM, ORE_OSMIUM },
    })

    -- Register tungsten
    internal.register_tungsten()

    -- Register lava to spawn more frequently the deeper you go
    internal.register_lava_pockets()

    -- Spawn ores in the world
    internal.spawn_coal_ore()
    internal.spawn_iron_ore()
    internal.spawn_diamond_ore()
    internal.spawn_uranium_ore()
    internal.spawn_osmium_ore()

    -- Add more dripstone blocks
    -- internal.register_dripstone_blocks()
end

-- Start the cave generator
function internal.add_custom_caves(filename)
    local fullname = minetest.get_modpath(minetest.get_current_modname()) .. "/" .. filename
    
    if ns_cavegen_init and ns_cavegen_init.register_cavegen_script then
        ns_cavegen_init.register_cavegen_script(fullname)
    else
        minetest.log("warning", "User uses deprecated version 1.0.0 of cave generator - please update soon.")
        minetest.register_mapgen_script(fullname)
    end
end

-- Create a deepcopy of a table
function internal.deepcopy(t)
    local new_t = {}

    for key, value in pairs(t) do
        if type(value) == "table" then
            new_t[key] = internal.deepcopy(value)
        else
            new_t[key] = value
        end
    end

    return new_t
end

-- Merge the second input table into the first
function internal.merge(t1, t2)
    for k, v in pairs(t2) do
        t1[k] = v
    end
    return t1
end

-- Generate a node name
function internal.node_name(prefix, name, suffix)
    if prefix ~= "" then
        prefix = prefix .. "_"
    end
    if suffix ~= "" then
        suffix = "_" .. suffix
    end

    return modname .. ":" .. prefix .. name:lower() .. suffix
end

-- Register blocks that interact with dripstone
function internal.register_dripstone_blocks()
    dripstone.register_catcher("water", "mcl_core:water_flowing", "mcl_core:water_flowing")
end

-- Register lava to spawn more frequently the deeper you go
function internal.register_lava_pockets()
    minetest.register_ore({
		ore_type       = "scatter",
		ore            = "mcl_core:lava_source",
		wherein        = internal.node_name("", "leagueschist", ""),
		clust_scarcity = 9000,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = LEAGUESCHIST_MIN,
		y_max          = LEAGUESCHIST_MAX,
	})
    minetest.register_ore({
		ore_type       = "scatter",
		ore            = "mcl_core:lava_source",
		wherein        = internal.node_name("", "bandgneiss", ""),
		clust_scarcity = 4000,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = BANDGNEISS_MIN,
		y_max          = BANDGNEISS_MAX,
	})
end

-- {
--      -- Material name. Required.
--      name = "Iron",
--
--      -- Time required to cook raw item to ingot. Defaults to 10. Optional.
--      cooktime = 10,
--
--      -- VoxeLibre blast resistance of a block. Optional.
--      -- For reference:
--      --     - Iron has a resistance of 6
--      --     - Diamond has a resistance of 6
--      blast_resistance = 6,
-- 
--      -- VoxeLibre hardness - how long it takes to break. Optional.
--      -- For reference:
--      --     - Iron has a hardness of 5
--      --     - Diamond has a hardness of 5
--      hardness = 3,
-- 
--      -- Extra params given to the full ingot block. Optional.
--      extra_params = { stack_max = 16 },
-- 
--      -- Extra params given to the raw ore block. Optional.
--      extra_raw_params = { sounds = {} },
-- }
function internal.register_ore(def)
    def.cooktime = def.cooktime or 10
    def.blast_resistance = def.blast_resistance or 6
    def.hardness = def.hardness or 5
    def.extra_params = def.extra_params or {}
    def.extra_raw_params = def.extra_raw_params or {}

    assert(type(def.name) == "string")
    assert(type(def.cooktime) == "number")
    assert(type(def.blast_resistance) == "number")
    assert(type(def.hardness) == "number")
    assert(type(def.extra_params) == "table")
    assert(type(def.extra_raw_params) == "table")

    local raw_node = internal.node_name("raw", def.name, "")
    local ingot_node = internal.node_name("", def.name, "ingot")
    local block_node = internal.node_name("", def.name, "block")
    local rblock_node = internal.node_name("raw", def.name, "block")

    minetest.register_craftitem(raw_node, {
        description = def.name .. " ingot",
        _doc_items_longdesc = "Raw version of " .. def.name .. ". It needs to melt before it can be used.",
        inventory_image = "ns_vl_caves_raw_" .. def.name:lower() .. ".png",
        stack_max = 64,
        groups = { craftitem = 1, blast_furnace_smeltable = 1 },
    })

    minetest.register_craftitem(ingot_node, {
        description = def.name .. " ingot",
        _doc_items_longdesc = "Molten version of " .. def.name .. " in a pure form.",
        inventory_image = "ns_vl_caves_" .. def.name:lower() .. "_ingot.png",
        stack_max = 64,
        groups = { craftitem = 1 },
    })

    minetest.register_node(rblock_node,
        internal.merge(
            {
                description = "Raw " .. def.name:lower() .. " block",
                _doc_items_longdesc = "Full block of raw " .. def.name:lower() .. ".",
                tiles = { "ns_vl_caves_raw_" .. def.name:lower() .. "_block.png" },
                is_ground_content = false,
                stack_max = 64,
                groups = { pickaxey = 2, building_block = 1 },
                sounds = mcl_sounds.node_sound_metal_defaults(),
                _mcl_blast_resistance = def.blast_resistance,
                _mcl_hardness = def.hardness,
            },
            def.extra_raw_params
        )
    )

    minetest.register_node(block_node, 
        internal.merge(
            {
                description = def.name .. " block",
                _doc_items_longdesc = "Full block of molten " .. def.name:lower() .. ".",
                tiles = { "ns_vl_caves_" .. def.name:lower() .. "_block.png" },
                is_ground_content = false,
                stack_max = 64,
                groups = { pickaxey = 2, building_block = 1 },
                sounds = mcl_sounds.node_sound_metal_defaults(),
                _mcl_blast_resistance = def.blast_resistance,
                _mcl_hardness = def.hardness,
            },
            def.extra_params
        )
    )

    minetest.register_craft({
        type = "cooking",
        output = ingot_node,
        recipe = raw_node,
        cooktime = def.cooktime,
    })

    minetest.register_craft({
        output = block_node,
        recipe = {
            {ingot_node, ingot_node, ingot_node},
            {ingot_node, ingot_node, ingot_node},
            {ingot_node, ingot_node, ingot_node},
        }
    })

    minetest.register_craft({
        output = ingot_node .. " 9",
        recipe = {
            {block_node},
        }
    })

    minetest.register_craft({
        output = rblock_node,
        recipe = {
            {raw_node, raw_node, raw_node},
            {raw_node, raw_node, raw_node},
            {raw_node, raw_node, raw_node},
        }
    })

    minetest.register_craft({
        output = raw_node .. " 9",
        recipe = {
            {rblock_node},
        }
    })
end

function internal.register_monster_spawn(name, biomes, interval, chance, aoc)
    return mcl_mobs:spawn_specific(
        "mobs_mc:" .. name, "overworld", "ground", biomes, 0, 7,
        interval, chance, aoc, mcl_vars.mg_overworld_min,
        mcl_vars.mg_overworld_max, nil, function(self, pos)
            minetest.debug("Spawned " .. name .. " at position " .. minetest.pos_to_string(pos))
        end
    )
end

-- Register a new deepslate-like stone type.
-- {
--      -- Name of the deepslate type. Required.
--      name = "deepslate",
--
--      -- Longer description about the node.
--      description = "Deepslate is a cool underground block!"
--
--      -- VoxeLibre blast resistance of a block. Optional.
--      -- For reference:
--      --     - Stone has a resistance of 6
--      --     - Deepslate has a resistance of 6
--      blast_resistance = 6,
-- 
--      -- VoxeLibre hardness - how long it takes to break. Optional.
--      -- For reference:
--      --     - Stone has a hardness of 1.5
--      --     - Deepslate has a hardness of 3
--      hardness = 3,
--
--      -- Ores that the stone can contain. Optional.
--      ores = {
--          { name = "iron"
--          , mined = "mcl_raw_ores:raw_iron"   -- Optional
--          , molten = "mcl_core:iron_ingot"    -- Optional
--          , pick_level = 2                    -- Optional
--          , xp = 1                            -- Optional
--          , fortune = true                    -- Optional
--          }
--      }
-- }
function internal.register_stone(def)
    -- Fill optional values
    def.blast_resistance = def.blast_resistance or 6
    def.hardness = def.hardness or 3
    def.ores = def.ores or {}
    def.fortune = def.fortune or false

    -- Clean stone definition
    assert(type(def.name) == "string")
    assert(def.description == nil or type(def.description) == "string")
    assert(type(def.blast_resistance) == "number")
    assert(type(def.hardness) == "number")
    assert(type(def.ores) == "table")
    assert(type(def.fortune) == "boolean")

    local std_node = internal.node_name("", def.name, "")
    local cobbled = internal.node_name("cobbled", def.name, "")

    -- Standard block
    minetest.register_node(std_node, {
        description = def.name,
        _doc_items_longdesc = def.description,
        tiles = { "ns_vl_caves_" .. def.name:lower() .. ".png" },
        is_ground_content = true,
        stack_max = 64,
        groups = { pickaxey = 1, stone = 1, building_block = 1, material_stone = 1 },
        drop = cobbled,
        sounds = mcl_sounds.node_sound_stone_defaults(),
        _mcl_blast_resistance = def.blast_resistance,
        _mcl_hardness = def.hardness,
        _mcl_silk_touch_drop = true,
    })

    -- Cobbled block
    minetest.register_node(cobbled, {
        description = "Cobbled " .. def.name:lower(),
        _doc_items_longdesc = "Cobbled " .. def.name:lower() .. " is a stone variant that functions similar to cobblestone or blackstone.",
        tiles = { "ns_vl_caves_cobbled_" .. def.name:lower() .. ".png" },
        is_ground_content = true,
        stack_max = 64,
        groups = { pickaxey = 1, stone = 1, building_block = 1, material_stone = 1 },
        sounds = mcl_sounds.node_sound_stone_defaults(),
        _mcl_blast_resistance = def.blast_resistance,
        _mcl_hardness = 2 * def.hardness / 3,
    })

    -- Ore blocks
    for _, ore in pairs(def.ores) do
        ore.extra_params = ore.extra_params or {}

        assert(type(ore.name) == "string")
        assert(ore.mined == nil or type(ore.mined) == "string")
        assert(ore.molten == nil or type(ore.molten) == "string")
        assert(ore.pick_level == nil or type(ore.pick_level) == "number")
        assert(ore.xp == nil or type(ore.xp) == "number")
        assert(type(ore.extra_params) == "table")

        local ore_name = internal.node_name("", def.name:lower(), "with_" .. ore.name:lower())
        local fortune
        if ore.fortune then
            fortune = mcl_core.fortune_drop_ore
        end

        minetest.register_node(ore_name, internal.merge(
            {
                description = def.name .. " with " .. ore.name:lower() .. " ore",
                _doc_items_longdesc = def.name .. " with " .. ore.name:lower() .. " ore is a variant of " .. ore.name .. " ore that can generate underground.",
                tiles = { "ns_vl_caves_" .. def.name:lower() .. "_with_" .. ore.name:lower() .. ".png" },
                is_ground_content = true,
                stack_max = 64,
                groups = { pickaxey = ore.pick_level, stone = 1, building_block = 1, material_stone = 1, blast_furnace_smeltable = 1 },
                drop = ore.mined,
                sounds = mcl_sounds.node_sound_stone_defaults(),
                _mcl_blast_resistance = def.blast_resistance / 2,
                _mcl_hardness = 2 * def.hardness,
                _mcl_silk_touch_drop = true,
                _mcl_fortune_drop = fortune,
            },
            ore.extra_params
        ))

        if ore.molten then
            minetest.register_craft({
                type = "cooking",
                output = ore.molten,
                recipe = ore_name,
                cooktime = 10,
            })
        end
    end
end

function internal.register_tungsten()
    local node_name = internal.node_name("", "tungsten", "")

    minetest.register_node(node_name, {
        description = "Tungsten",
        _doc_items_longdesc = "Tungsten is a heavy type of stone that is built from large compressions of heavy particles.",
        tiles = { "ns_vl_caves_tungsten.png" },
        is_ground_content = true,
        stack_max = 64,
        groups = { pickaxey = 2, stone = 1, building_block = 1, material_stone = 1 },
        sounds = mcl_sounds.node_sound_stone_defaults(),
        _mcl_blast_resistance = 10,
        _mcl_hardness = 7,
    })

    minetest.register_ore({
        ore_type       = "blob",
        ore            = node_name,
        wherein        = {
            internal.node_name("", "leagueschist", ""),
            internal.node_name("", "bandgneiss", "")
        },
        clust_scarcity = 10*10*10,
        clust_num_ores = 58,
        clust_size     = 7,
        y_min          = BANDGNEISS_MIN,
        y_max          = LEAGUESCHIST_MAX,
        noise_params = {
            offset  = 0,
            scale   = 1,
            spread  = {x=250, y=250, z=250},
            seed    = 12345,
            octaves = 3,
            persist = 0.6,
            lacunarity = 2,
            flags = "defaults",
        }
    })
end

-- Lower the world depth
function internal.overworld_depth_to(h)
    local oldh = mcl_vars.mg_overworld_min
    local diff = h - mcl_vars.mg_overworld_min

    -- Lower barriers below the overworld
    mcl_vars.mg_bedrock_overworld_max = mcl_vars.mg_bedrock_overworld_max + diff
    mcl_vars.mg_overworld_min = mcl_vars.mg_overworld_min + diff
    mcl_vars.mg_end_max = mcl_vars.mg_end_max + diff
    mcl_vars.mg_realm_barrier_overworld_end_max = mcl_vars.mg_realm_barrier_overworld_end_max + diff
    mcl_vars.mg_realm_barrier_overworld_end_min = mcl_vars.mg_realm_barrier_overworld_end_min + diff

    -- Lower other variables
    mcl_vars.mg_lava_overworld_max = mcl_vars.mg_lava_overworld_max + diff

    -- -- DEBUG: Find potentially missing variables
    -- for k, v in pairs(mcl_vars) do
    --     if type(v) == "number" and h < v and v < oldh + 20 then
    --         minetest.debug(k .. " = " .. tostring(v))
    --     end
    -- end

    return oldh
end

-- The mcl_biomes mod is written in a way where it's nearly impossible to
-- influence from the outside. For this reason, this function is a hardcoded
-- method of getting in the way of the biome definitions, creating underground
-- biomes that allow for different layers of stone, and more.
function internal.overwrite_register_biome()

    minetest.register_biome = function(def)
        if def.y_min == WORLD_DEPTH then
            local slate_def = internal.deepcopy(def)
            local schist_def = internal.deepcopy(def)
            local gneiss_def = internal.deepcopy(def)

            def.y_min = STONE_MIN

            slate_def.name = slate_def.name .. "_deepslate"
            slate_def.y_max = DEEPSLATE_MAX
            slate_def.y_min = DEEPSLATE_MIN
            slate_def.node_stone = "mcl_deepslate:deepslate"
            slate_def.vertical_blend = 8

            schist_def.name = schist_def.name .. "_deepschist"
            schist_def.y_max = LEAGUESCHIST_MAX
            schist_def.y_min = LEAGUESCHIST_MIN
            schist_def.node_stone = internal.node_name("", "leagueschist", "")
            schist_def.vertical_blend = 8

            gneiss_def.name = gneiss_def.name .. "_deepgneiss"
            gneiss_def.y_max = BANDGNEISS_MAX
            gneiss_def.y_min = BANDGNEISS_MIN
            gneiss_def.node_stone = internal.node_name("", "bandgneiss", "")
            gneiss_def.vertical_blend = 8

            OLD_REGISTER_BIOME(def)
            OLD_REGISTER_BIOME(slate_def)
            OLD_REGISTER_BIOME(schist_def)
            OLD_REGISTER_BIOME(gneiss_def)

            local biome_names = { slate_def.name, schist_def.name, gneiss_def.name }

            internal.register_monster_spawn("zombie", biome_names, 30, 1000, 4)
            internal.register_monster_spawn("rover", biome_names, 30, 100, 2)
            internal.register_monster_spawn("skeleton", biome_names, 20, 800, 2)
            internal.register_monster_spawn("spider", biome_names, 30, 1000, 2)
            internal.register_monster_spawn("stalker", biome_names, 20, 1000, 2)
            internal.register_monster_spawn("villager_zombie", biome_names, 30, 50, 4)
            internal.register_monster_spawn("zombie", biome_names, 30, 1000, 4)
            internal.register_monster_spawn("baby_zombie", biome_names, 30, 50, 4)
        else
            return OLD_REGISTER_BIOME(def)
        end
    end
end

function internal.spawn_coal_ore()
    local deepslate = "mcl_deepslate:deepslate"
    local deepslate_ore = "mcl_deepslate:deepslate_with_coal"
    local leagueschist = internal.node_name("", "leagueschist", "")
    local leagueschist_ore = internal.node_name("", "leagueschist", "with_coal")

    minetest.register_ore({
		ore_type       = "scatter",
		ore            = deepslate_ore,
		wherein        = deepslate,
		clust_scarcity = 525*3,
		clust_num_ores = 5,
		clust_size     = 3,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = deepslate_ore,
		wherein        = deepslate,
		clust_scarcity = 510*3,
		clust_num_ores = 8,
		clust_size     = 3,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = deepslate_ore,
		wherein        = deepslate,
		clust_scarcity = 500*3,
		clust_num_ores = 12,
		clust_size     = 3,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})

    minetest.register_ore({
		ore_type       = "scatter",
		ore            = leagueschist_ore,
		wherein        = leagueschist,
		clust_scarcity = 525*3,
		clust_num_ores = 5,
		clust_size     = 3,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = leagueschist_ore,
		wherein        = leagueschist,
		clust_scarcity = 510*3,
		clust_num_ores = 8,
		clust_size     = 3,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = leagueschist_ore,
		wherein        = leagueschist,
		clust_scarcity = 500*3,
		clust_num_ores = 12,
		clust_size     = 3,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
end

function internal.spawn_diamond_ore()
    local deepslate = "mcl_deepslate:deepslate"
    local deepslate_ore = "mcl_deepslate:deepslate_with_diamond"
    local leagueschist = internal.node_name("", "leagueschist", "")
    local leagueschist_ore = internal.node_name("", "leagueschist", "with_diamond")
    local bandgneiss = internal.node_name("", "bandgneiss", "")
    local bandgneiss_ore = internal.node_name("", "bandgneiss", "with_diamond")

    -- DEEPSLATE
	-- Rare spawn
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = deepslate_ore,
		wherein        = deepslate,
		clust_scarcity = 20000,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = deepslate_ore,
		wherein        = deepslate,
		clust_scarcity = 20000,
		clust_num_ores = 2,
		clust_size     = 2,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})

    -- LEAGUESCHIST
    -- Common spawn
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = leagueschist_ore,
		wherein        = leagueschist,
		clust_scarcity = 10000,
		clust_num_ores = 4,
		clust_size     = 3,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = leagueschist_ore,
		wherein        = leagueschist,
		clust_scarcity = 5000,
		clust_num_ores = 2,
		clust_size     = 2,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = leagueschist_ore,
		wherein        = leagueschist,
		clust_scarcity = 10000,
		clust_num_ores = 8,
		clust_size     = 3,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})

    -- BANDGNEISS
	-- Rare spawn
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = bandgneiss_ore,
		wherein        = bandgneiss,
		clust_scarcity = 20000,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = bandgneiss_ore,
		wherein        = bandgneiss,
		clust_scarcity = 20000,
		clust_num_ores = 2,
		clust_size     = 2,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
end

function internal.spawn_iron_ore()
    local deepslate = "mcl_deepslate:deepslate"
    local deepslate_ore = "mcl_deepslate:deepslate_with_iron"
    local leagueschist = internal.node_name("", "leagueschist", "")
    local leagueschist_ore = internal.node_name("", "leagueschist", "with_iron")

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = deepslate_ore,
		wherein        = deepslate,
		clust_scarcity = 830,
		clust_num_ores = 5,
		clust_size     = 3,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = deepslate_ore,
		wherein        = deepslate,
		clust_scarcity = 1660,
		clust_num_ores = 4,
		clust_size     = 2,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = leagueschist_ore,
		wherein        = leagueschist,
		clust_scarcity = 830,
		clust_num_ores = 5,
		clust_size     = 3,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = leagueschist_ore,
		wherein        = leagueschist,
		clust_scarcity = 1660,
		clust_num_ores = 4,
		clust_size     = 2,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = leagueschist_ore,
		wherein        = leagueschist,
		clust_scarcity = 770,
		clust_num_ores = 3,
		clust_size     = 2,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
end

function internal.spawn_osmium_ore()
    local bandgneiss = internal.node_name("", "bandgneiss", "")
    local bandgneiss_ore = internal.node_name("", "bandgneiss", "with_osmium")

    -- Rare spawn
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = bandgneiss_ore,
		wherein        = bandgneiss,
		clust_scarcity = 20000,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = bandgneiss_ore,
		wherein        = bandgneiss,
		clust_scarcity = 20000,
		clust_num_ores = 2,
		clust_size     = 2,
		y_min          = mcl_vars.mg_overworld_min,
		y_max          = mcl_vars.mg_overworld_max,
	})
end

function internal.spawn_uranium_ore()
    local leagueschist = internal.node_name("", "leagueschist", "")
    local leagueschist_ore = internal.node_name("", "leagueschist", "with_uranium")
    local bandgneiss = internal.node_name("", "bandgneiss", "")
    local bandgneiss_ore = internal.node_name("", "bandgneiss", "with_uranium")

    -- LEAGUESCHIST
    -- Rare spawn
    minetest.register_ore({
        ore_type       = "scatter",
        ore            = leagueschist_ore,
        wherein        = leagueschist,
        clust_scarcity = 20000,
        clust_num_ores = 1,
        clust_size     = 1,
        y_min          = mcl_vars.mg_overworld_min,
        y_max          = mcl_vars.mg_overworld_max,
    })
    minetest.register_ore({
        ore_type       = "scatter",
        ore            = leagueschist_ore,
        wherein        = leagueschist,
        clust_scarcity = 20000,
        clust_num_ores = 2,
        clust_size     = 2,
        y_min          = mcl_vars.mg_overworld_min,
        y_max          = mcl_vars.mg_overworld_max,
    })
        
    -- BANDGNEISS
    -- Common spawn
    minetest.register_ore({
        ore_type       = "scatter",
        ore            = bandgneiss_ore,
        wherein        = bandgneiss,
        clust_scarcity = 10000,
        clust_num_ores = 4,
        clust_size     = 3,
        y_min          = mcl_vars.mg_overworld_min,
        y_max          = mcl_vars.mg_overworld_max,
    })
    minetest.register_ore({
        ore_type       = "scatter",
        ore            = bandgneiss_ore,
        wherein        = bandgneiss,
        clust_scarcity = 5000,
        clust_num_ores = 2,
        clust_size     = 2,
        y_min          = mcl_vars.mg_overworld_min,
        y_max          = mcl_vars.mg_overworld_max,
    })
    minetest.register_ore({
        ore_type       = "scatter",
        ore            = bandgneiss_ore,
        wherein        = bandgneiss,
        clust_scarcity = 10000,
        clust_num_ores = 8,
        clust_size     = 3,
        y_min          = mcl_vars.mg_overworld_min,
        y_max          = mcl_vars.mg_overworld_max,
    })
end

-- Prevent v7 from spawning large caves underground
function internal.stop_v7_caverns()
    local s = minetest.get_mapgen_setting("mgv7_spflags")

    if s then
        s = s:gsub(", nocaverns", "")
        s = s:gsub(", caverns"  , "")
        s = s:gsub("nocaverns, ", "")
        s = s:gsub("caverns, "  , "")
        s = s:gsub("nocaverns"  , "")
        s = s:gsub("caverns"    , "")
        
        if s == "" then
            minetest.set_mapgen_setting("mgv7_spflags", "nocaverns", true)
        else
            minetest.set_mapgen_setting("mgv7_spflags", s .. ", nocaverns", true)
        end
    end
end

-- Run main function
internal.init()

