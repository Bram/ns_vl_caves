local WORLD_DEPTH = -3000

function ns_cavegen.cave_vastness(pos)
    if pos.y > -5 or pos.y < WORLD_DEPTH then
        return 0
    end

    local y = math.abs(pos.y)

    -- Sinusoid shape increase
    local amplitude = math.sqrt(y / math.abs(WORLD_DEPTH)) / 2
    local period = 1000 / (2 * math.pi)
    local offset = 250

    local sinusoid_shape = amplitude * (math.sin((y - offset) / period) + 1)

    -- Slow increase
    local slow_increase = y / math.abs(WORLD_DEPTH)

    return 0.2 * slow_increase + 0.8 * sinusoid_shape
end

-- SHAPES

-- Layer 1: Deepslate layer

ns_cavegen.register_shape({
    name = "ns_vl_caves:layer_1_bulbs",
    noise_params = {
        offset = 0,
        scale = 1,
        spread = { x = 30, y = 10, z = 30 },
        seed = 90658795,
        octaves = 3,
        persistence = 0.75,
        lacunarity = 0.8,
        flags = "eased"
    },
    y_max = -100,
    y_min = -900,
    connectivity_point = 71,
    verticality_point = 43,
})
ns_cavegen.register_shape({
    name = "ns_vl_caves:layer_1_pods",
    noise_params = {
        offset = -10,
        scale = 11,
        spread = { x = 5, y = 10, z = 5 },
        seed = 90658795,
        octaves = 2,
        persistence = 0.75,
        lacunarity = 0.8,
        flags = "eased"
    },
    y_max = -100,
    y_min = -900,
    connectivity_point = 8,
    verticality_point = 23,
})

-- Layer 2: Leagueschist layer

ns_cavegen.register_shape({
    name = "ns_vl_caves:layer_2_bulbs",
    noise_params = {
        offset = 0,
        scale = 1,
        spread = { x = 30, y = 10, z = 30 },
        seed = 90658795,
        octaves = 3,
        persistence = 0.75,
        lacunarity = 0.8,
        flags = "eased"
    },
    y_max = -1000,
    y_min = -1950,
    connectivity_point = 71,
    verticality_point = 43,
})
ns_cavegen.register_shape({
    name = "ns_vl_caves:layer_2_swirls",
    noise_params = {
        offset = 0,
        scale = 1,
        spread = { x = 10, y = 50, z = 10 },
        seed = 19220,
        octaves = 3,
        persistence = 0.9,
        lacunarity = 0.9,
        flags = "eased",
    },
    y_max = -900,
    y_min = -2100,
    connectivity_point = 38,
    verticality_point = 63,
})

-- Layer 3: Bandgneiss layer

ns_cavegen.register_shape({
    name = "ns_vl_caves:layer_3_cliffs",
    noise_params = {
        offset = 0,
        scale = 1,
        spread = { x = 50, y = 200, z = 50 },
        seed = 1854392,
        octaves = 3,
        persistence = 0.8,
        lacunarity = 3.0,
        flags = "",
    },
    y_max = -1500,
    y_min = -3000,
    connectivity_point = 45,
    verticality_point = 89,
})
ns_cavegen.register_shape({
    name = "ns_vl_caves:layer_3_horizontal_spaghetti",
    noise_params = {
        offset = -0.3,
        scale = 0.7,
        spread = { x = 300, y = 10, z = 40 },
        seed = 73405,
        octaves = 3,
        persistence = 0.3,
        lacunarity = 3.0,
        flags = "eased",
    },
    y_max = -2250,
    y_min = -3000,
    connectivity_point = 83,
    verticality_point = 5,
})
ns_cavegen.register_shape({
    name = "ns_vl_caves:layer_3_open_space",
    noise_params = {
        offset = 0.75,
        scale = 0.3,
        spread = { x = 100, y = 100, z = 100 },
        seed = 333078,
        octaves = 1,
        persistence = 0.1,
        lacunarity = 10.0,
        flags = "",
    },
    y_max = -2250,
    y_min = -2800,
    connectivity_point = 100,
    verticality_point = 100,
})
ns_cavegen.register_shape({
    name = "ns_vl_caves:layer_3_sideways_spaghetti",
    noise_params = {
        offset = -0.3,
        scale = 0.7,
        spread = { x = 10, y = 40, z = 300 },
        seed = 73405,
        octaves = 3,
        persistence = 0.3,
        lacunarity = 3.0,
        flags = "eased",
    },
    y_min = -2250,
    y_max = -3000,
    connectivity_point = 82,
    verticality_point = 4,
})

-----------------------------------------------------------
-----------------------------------------------------------

-- ns_cavegen.register_shape({
--     name = "ns_vl_caves:bubbles",
--     noise_params = {
--         offset = 0.2,
--         scale = 0.6,
--         spread = { x = 100, y = 100, z = 100 },
--         seed = 248039,
--         octaves = 2,
--         persistence = 0.6,
--         lacunarity = 2.0,
--         flags = "eased"
--     },
--     connectivity_point = 10,
--     verticality_point = 20,
-- })

-- BIOMES

-- ns_cavegen.register_biome({
--     name = "ns_vl_caves:light_floor",
--     -- node_floor = "mcl_crimson:shroomlight",
--     node_dust = "mcl_core:light_14",
--     node_wall = "mcl_nether:glowstone",
--     node_roof = "mcl_crimson:shroomlight",
--     heat_point = 42,
--     humidity_point = 33,
-- })

-- ns_cavegen.register_biome({
--     name = "ns_vl_caves:dark_floor",
--     node_floor = "mcl_core:cobble",
--     node_wall = "mcl_core:cobble",
--     heat_point = 73,
--     humidity_point = 23,
-- })
